import React from 'react';

class TodoList extends React.Component{
  
  render(){
 const itemsleft=0;
 const items = this.props.mytasklist.map((item,i) => {
   let task_id = 'task_'+i;
   return(

<li key={i} id={task_id} className={item.completed ? 'active':'passive'}>

<span className="id">{i+1}.       </span>
<span className="title">{item.title}</span>
<span className="type" onClick={e => this.props.doneTask(e.target.parentNode.id)} />
<span className="delete" onClick={e =>this.props.removeTask(e.target.parentNode.id)}  />
</li>
   )
 });
    return(
      <div>
      <div className="todo-list type1">
      <ul>{items}</ul>
      </div>


      <div className="todo-filter">
      <div className="left">
      <span>{itemsleft} items left</span>
      </div>

      <div className="right">
      <ul>
      <li><span className="active">All</span></li>
      <li><span>Active</span></li>
      <li><span>Completed</span></li>
      </ul>
      </div>

      </div>

      </div>
)}}

export default TodoList;
