import React,{Component} from 'react';
import TodoList from './todolist'
import TodoForm from './Form';
import Header from './inc/header';
import Footer from './inc/footer';


class Page extends Component{
  constructor(){
      super();
    this.state =
    {
      mytasklist: [{text:"Bug fixed",status:"passive"},{text:"Read blog",status:"passive"}],
      fakeapilist :[]
    };
    //this.addTask = this.addTask.bind(this);
    //this.removeTask = this.removeTask.bind(this);
    //this.doneTask = this.doneTask.bind(this);
  }

addTask= (val)=>{
  //console.log(this.state.mytasklist)
  let updatedList =this.state.fakeapilist;
  updatedList.push({title:val,completed:false});
  this.setState({fakeapilist: updatedList});
//console.log(this.state.mytasklist)
}

 doneTask = (task_id) =>{
  task_id =task_id.replace('task_','');
  let updatedList =this.state.fakeapilist;
  let newStatus =true;
  let currentStatus = updatedList[task_id].completed;
  if(currentStatus){
    newStatus = false;
  }
  updatedList[task_id].completed= newStatus;
  this.setState({fakeapilist:updatedList});
}

removeTask = (task_id) => {
  task_id =task_id.replace('task_','');
  let updatedList =this.state.fakeapilist;
  updatedList.splice(task_id,1);
  this.setState({fakeapilist:updatedList});
}



  componentDidMount() {
   fetch("https://jsonplaceholder.typicode.com/todos/")
     .then(res => res.json())
     .then(
       (result) => {
         this.setState({
           fakeapilist:result.splice(0,3)
         })
         console.log('result',this.state.fakeapilist);
       },
       (error) => {
         this.setState({
           isLoaded: true,
           error
         });

          console.log('error',error);
       }
     )
 }

render(){

  return(
        <div className="content">
        <Header/>
        <div>
        <TodoForm addTask={this.addTask}/>
        <TodoList mytasklist={this.state.fakeapilist}
        doneTask={this.doneTask}
        removeTask={this.removeTask} />
        </div>
        <Footer/>
        </div>
  )
}
}

export default Page;
